# Dialer-Front-Project2
Front-End Angular application made for [Dialer project](https://bitbucket.org/mkuczkowski/dialer-project1) that allows users to make phone calls through browser.
### Prerequisites
* [Node.js (v8.x)](https://nodejs.org/en/) - JavaScript runtime environment
* [npm (v5.x)](https://www.npmjs.com/) - Package manager for Node.js
* [Angular (v7)](https://github.com/angular/angular-cli) - TypeScript-based web application framework  
### Getting started
Application has been made with [Angular CLI v7](https://github.com/angular/angular-cli).   
If you want to install Angular globally you can use command:  
```sh
  npm install -g @angular/cli
```
Download Dialer Back-End app available [here](https://bitbucket.org/mkuczkowski/dialer-project1) and follow [these instructions](https://bitbucket.org/mkuczkowski/dialer-project1/src/master/README.md) to configure it.  
If your backend app is running correctly, the Command Interpreter displays:  
```sh
  app listening on port 3000
```
Open second terminal and navigate to front-end app directory using cd command and run npm install command:  
```sh
  cd path-to-front
  npm install
```
To start front-end app run following command:
```sh
  ng serve
```
Command Interpreter should display:
```sh
  ** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
```
Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.  
After typing correct nine-digit phone number, app will allow you to make a call by clicking "Call" button.
